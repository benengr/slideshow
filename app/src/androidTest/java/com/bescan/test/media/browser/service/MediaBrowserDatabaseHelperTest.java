package com.bescan.test.media.browser.service;

import com.bescan.media.browser.service.MediaBrowserDatabaseHelper;

import junit.framework.TestCase;


/**
 * Created by banderson on 1/16/15.
 */
public class MediaBrowserDatabaseHelperTest extends TestCase {

    public MediaBrowserDatabaseHelperTest() {
        super();
    }
    public void testPrintSQLCreate() {
        String sql = MediaBrowserDatabaseHelper.Songs.SQL_CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.Artists.CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.Albums.SQL_CREATE_ALBUM_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.AudioBook.CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.Bands.SQL_CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.Playlists.SQL_CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.PlaylistMap.SQL_CREATE_TABLE;
        System.out.println(sql);
        sql = MediaBrowserDatabaseHelper.Genre.CREATE_TABLE;
        System.out.println(sql);
        assertTrue(sql.equals(""));

    }
}
