package com.bescan.test.media.player;

import android.media.MediaMetadataRetriever;
import android.os.Looper;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.bescan.media.player.service.AndroidMediaPlayer;
import com.bescan.media.SongInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeoutException;

/**
 * Test suite to test the AndroidMediaPlayer class
 * Created by banderson on 9/30/14.
 */
public class AndroidMediaPlayerTest extends AndroidTestCase {
    private static final int PLAY_TIME = 10000; // Ten Seconds
    private static final int PAUSE_TIME = 5000; //
    protected AndroidMediaPlayer mMediaPlayer;
    private boolean mInitialized = false;
    private final Object lock = new Object();
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        initializeMessageLooper();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        terminateMessageLooper();

    }

    @MediumTest
    public void testAddSong() {
        ArrayList<SongInfo> l = getSongList();
        assertTrue(l.size() > 0);

        for(SongInfo inf : l) {
            //mMediaPlayer.AddSong(inf);
        }

        //assertTrue(mMediaPlayer.GetPlaylistCount() == l.size());
    }

    @MediumTest
    public void testAddSongCollection() {
        ArrayList<SongInfo> l = getSongList();
        assertTrue(l.size() > 0);
        //mMediaPlayer.AddSong(l);
        //assertTrue(mMediaPlayer.GetPlaylistCount() == l.size());
    }

    @MediumTest
    /**
     * This test tests when a user selects a different song to play while a song is already playing
     */
    public void testPlayWhilePlaying() {
        ArrayList<SongInfo> l = getSongList();
        //mMediaPlayer.AddSong(l);
        Random rng = new Random();
        int pos1, pos2;
        int idletime = 5000;
        SongInfo current;
        for(int i = 0; i < 10; i++) {
            int track = rng.nextInt(l.size());
            mMediaPlayer.play(track);
            try {
                mMediaPlayer.waitForState(AndroidMediaPlayer.PlayerState.PLAYING, 1000);
            } catch (TimeoutException e) {
                assertTrue("Timeout waiting to move to PLAYING state", false);
            }
            assertEquals("Media player not in playing state after Play() called",
                    AndroidMediaPlayer.PlayerState.PLAYING, mMediaPlayer.getPlayerState());
            //current = mMediaPlayer.GetCurrentSong();
            //verifySongInfo(l.get(track), current);
            pos1 = mMediaPlayer.getCurrentPosition();

            Wait(idletime);
            pos2 = mMediaPlayer.getCurrentPosition();
            CheckEllapsedTime(pos1, pos2, idletime);
        }
    }
    @MediumTest
    public void testStateMachine() {
        ArrayList<SongInfo> l = getSongList();
        SongInfo current;
        int pos1, pos2;
        assertTrue(l.size() > 0);
        //mMediaPlayer.AddSong(l);
        //assertTrue(mMediaPlayer.GetPlaylistCount() == l.size());
        assertTrue(mMediaPlayer.getPlayerState() == AndroidMediaPlayer.PlayerState.STOPPED);

        // Test Stopped To Playing
        mMediaPlayer.play();
        try {
            mMediaPlayer.waitForState(AndroidMediaPlayer.PlayerState.PLAYING, 10000);
        } catch (TimeoutException e) {
            assertTrue("Timeout waiting to move to PLAYING state", false);
        }
        pos1 = mMediaPlayer.getCurrentPosition();
        //current = mMediaPlayer.GetCurrentSong();
        //assertNotNull(current);
        //assertEquals(l.get(0).getTrackName(), current.getTrackName());
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.PLAYING, mMediaPlayer.getPlayerState());
        Wait(PLAY_TIME);
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.PLAYING, mMediaPlayer.getPlayerState());
        pos2 = mMediaPlayer.getCurrentPosition();
        CheckEllapsedTime(pos1, pos2, PLAY_TIME);

        // Test Playing to Paused
        mMediaPlayer.pause();
        assertEquals("Media player not in playing state after Paused() called",
                AndroidMediaPlayer.PlayerState.PAUSED, mMediaPlayer.getPlayerState());
        //current = mMediaPlayer.GetCurrentSong();
        //assertNotNull(current);
        //assertEquals(l.get(0).getTrackName(), current.getTrackName());
        pos1 = mMediaPlayer.getCurrentPosition();
        Wait(PAUSE_TIME);
        assertEquals("Media player not in playing state after Paused() called",
                AndroidMediaPlayer.PlayerState.PAUSED, mMediaPlayer.getPlayerState());
        pos2 = mMediaPlayer.getCurrentPosition();
        assertTrue("No Time should have ellapsed while paused", pos1 == pos2);

        // Test Paused to Playing
        mMediaPlayer.play();
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.PLAYING, mMediaPlayer.getPlayerState());
        Wait(PLAY_TIME);
        //current = mMediaPlayer.GetCurrentSong();
        //assertNotNull(current);
        //assertEquals(l.get(0).getTrackName(), current.getTrackName());
        pos2 = mMediaPlayer.getCurrentPosition();
        CheckEllapsedTime(pos1, pos2, PLAY_TIME);

        // Test Playing to Stopped
        mMediaPlayer.stop();
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.STOPPED, mMediaPlayer.getPlayerState());
        assertTrue("Ellapsed Time should be set to 0 on STOP", mMediaPlayer.getCurrentPosition() == 0);

        // Test Stop to Play After Play
        mMediaPlayer.play();
        //current = mMediaPlayer.GetCurrentSong();
        //assertNotNull(current);
        //assertEquals(l.get(0).getTrackName(), current.getTrackName());
        try {
            mMediaPlayer.waitForState(AndroidMediaPlayer.PlayerState.PLAYING, 10000);
        } catch (TimeoutException e) {
            assertTrue("Thread interrupted", false);
        }
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.PLAYING, mMediaPlayer.getPlayerState());
        pos1 = mMediaPlayer.getCurrentPosition();
        Wait(PLAY_TIME);
        pos2 = mMediaPlayer.getCurrentPosition();
        CheckEllapsedTime(pos1, pos2, PLAY_TIME);

        // Test Paused to Stop
        mMediaPlayer.pause();
        pos1 = mMediaPlayer.getCurrentPosition();
        Wait(PAUSE_TIME);
        pos2 = mMediaPlayer.getCurrentPosition();
        assertTrue("No Time should have ellapsed while paused", pos1 == pos2);
        mMediaPlayer.stop();
        assertEquals("Media player not in playing state after Play() called",
                AndroidMediaPlayer.PlayerState.STOPPED, mMediaPlayer.getPlayerState());
        assertTrue("Ellapsed Time should be set to 0 on STOP", mMediaPlayer.getCurrentPosition() == 0);

        // Test Stop to Play after Pause
        mMediaPlayer.play();
        try {
            mMediaPlayer.waitForState(AndroidMediaPlayer.PlayerState.PLAYING, 10000);
        } catch (TimeoutException e) {
            assertTrue("Thread interrupted", false);
        }
        assertEquals("Media player not in playing state after Play() called",
                mMediaPlayer.getPlayerState(), AndroidMediaPlayer.PlayerState.PLAYING);
        pos1 = mMediaPlayer.getCurrentPosition();
        Log.i(TAG, "Elapsed Time: " + pos1);
        assertTrue(pos1 < PLAY_TIME);
        //current = mMediaPlayer.GetCurrentSong();
        ///assertNotNull(current);
        //assertEquals(l.get(0).getTrackName(), current.getTrackName());


    }

    private ArrayList<SongInfo> getSongList() {
        ArrayList<SongInfo> songs = new ArrayList<SongInfo>();
        File dir = new File(testDirectory);
        if(dir.listFiles().length < 1)
            return songs;

        for(File f: dir.listFiles()) {
            if(f.isFile()) {
                if(f.getName().contains(".mp3")) {
                    songs.add(createSongInfoFromFile(f));
                }
            }
        }
        Log.i(TAG, "Found " + songs.size() + " songs");
        return songs;
    }

    private SongInfo createSongInfoFromFile(File f) {
        SongInfo si = new SongInfo();

        MediaMetadataRetriever mr = new MediaMetadataRetriever();
        mr.setDataSource(f.getPath());
        si.setAlbum(mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
        si.setArtist(mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
        si.setFilename(f.getAbsolutePath());
        si.setTrackName(mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));

        try {
            String t = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER);
            si.setTrackNumber(Integer.parseInt(t.split("/")[0]));
        } catch (Exception e) {
            si.setTrackNumber(0);
        }
        return si;

    }
    private static final String TAG = "AndroidMediaPlayerTest";
    private static final String testDirectory = "/storage/sdcard1/test/";
    private Looper mLooper;
    private Thread looperThread;
    /*
     * Initializes the message looper so that the MediaPlayer object can
     * receive the callback messages.
     */
    private void initializeMessageLooper() {
        looperThread = new Thread() {
            @Override
            public void run() {
                // Set up a looper to be used by mMediaPlayer.
                Looper.prepare();

                // Save the looper so that we can terminate this thread
                // after we are done with it.
                mLooper = Looper.myLooper();

                mMediaPlayer = new AndroidMediaPlayer();

                synchronized(lock) {
                    mInitialized = true;
                    lock.notify();
                }

                /*
                Handler mHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        Log.v(TAG, "handleMessage: Received Message");
                    }
                };
                */
                Looper.loop();  // Blocks forever until Looper.quit() is called.
                synchronized (lock) {
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                    mInitialized = false;
                }
                Log.v(TAG, "initializeMessageLooper: quit.");
            }
        };

        looperThread.start();

        while(!mInitialized) {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void terminateMessageLooper() {
        mLooper.quit();
        try {
            looperThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void Wait(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            assertTrue("Test Interrupted!", false);
        }
    }

    private void CheckEllapsedTime(int pos1, int pos2, int expected) {
        int ellapsed = Math.abs(pos2 - pos1);
        int error = Math.abs(ellapsed - expected);
        double allowed = expected * 0.1;

        Log.v(TAG, String.format("Checking Ellapsed time Start: %d, Stop: %d Ellapsed: %d, Error %d, Expected: %d",
                pos1, pos2, ellapsed, error, expected));
        assertTrue("Error in ellapsed time", error < allowed);
    }

    public void verifySongInfo(SongInfo expected, SongInfo actual) {
        assertEquals(expected.getTrackName(), actual.getTrackName());
    }
}
