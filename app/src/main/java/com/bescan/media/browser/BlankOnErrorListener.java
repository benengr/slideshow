package com.bescan.media.browser;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by Ben on 10/17/2014.
 */
public class BlankOnErrorListener implements ImageLoadingListener {
    private static final String TAG = "ImageLoader(BlankOnFailure)";
    @Override
    public void onLoadingStarted(String imageUri, View view) {

    }

    @Override
    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
        if(imageUri == null) {
            Log.i(TAG, "Error loading image because the URI was null");
        } else if(failReason == null) {
            Log.w(TAG, "Error Loading '" + imageUri + "', but no reason availble");
        } else {

            Log.i(TAG, "Error loading '" + imageUri + "'. Type: " + failReason.getType());
        }
        ((ImageView)view).setImageDrawable(null);
    }

    @Override
    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

    }

    @Override
    public void onLoadingCancelled(String imageUri, View view) {

    }
}
