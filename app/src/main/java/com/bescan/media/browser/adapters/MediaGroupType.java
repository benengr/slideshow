package com.bescan.media.browser.adapters;

import android.net.Uri;
import android.provider.MediaStore;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by banderson on 10/18/14.
 */
public enum MediaGroupType {
    ALBUM,
    ARTIST,
    GENRE,
    PLAYLIST,
    AUDIOBOOK,
    PODCAST;


    public static final Map<MediaGroupType, Uri> uriMap;
    static {
        HashMap<MediaGroupType, Uri> aMap = new HashMap<MediaGroupType, Uri>();
        aMap.put(MediaGroupType.ALBUM, MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI);
        aMap.put(MediaGroupType.ARTIST, MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI);
        aMap.put(MediaGroupType.GENRE, MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI);
        aMap.put(MediaGroupType.PLAYLIST, MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI);
        aMap.put(MediaGroupType.AUDIOBOOK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        aMap.put(MediaGroupType.PODCAST, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        uriMap = Collections.unmodifiableMap(aMap);
    }
    public static final Map<MediaGroupType, String>titleMap;
    static {
        HashMap<MediaGroupType, String> aMap = new HashMap<MediaGroupType, String>();
        aMap.put(MediaGroupType.ALBUM, MediaStore.Audio.Albums.ALBUM);
        aMap.put(MediaGroupType.ARTIST, MediaStore.Audio.Artists.ARTIST);
        aMap.put(MediaGroupType.GENRE, MediaStore.Audio.Genres.NAME);
        aMap.put(MediaGroupType.PLAYLIST, MediaStore.Audio.Playlists.NAME);
        titleMap = Collections.unmodifiableMap(aMap);
    }
    public static final Map<MediaGroupType, String>subtitleMap;
    static {
        HashMap<MediaGroupType, String> aMap = new HashMap<MediaGroupType, String>();
        aMap.put(MediaGroupType.ALBUM, MediaStore.Audio.Albums.ARTIST);
        subtitleMap = Collections.unmodifiableMap(aMap);
    }
    public static final Map<MediaGroupType, String>imageUriMap;
    static {
        HashMap<MediaGroupType, String> aMap = new HashMap<MediaGroupType, String>();
        aMap.put(ALBUM, MediaStore.Audio.Albums.ALBUM_ART);
        imageUriMap = Collections.unmodifiableMap(aMap);
    }
}
