package com.bescan.media.browser.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.ArrayList;

/**
 * This is an asynchronous loader that returns a cursor pointing to all the albums in the
 * MediaStore that have podcasts
 *
 * Created by banderson on 10/20/14.
 */
public class PodcastLoader extends AsyncTaskLoader<Cursor> {
    protected Context mContext;
    private static final String TAG = "PodcastLoader";
    public PodcastLoader(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Cursor loadInBackground() {

        // First Query the db for all songs that are podcasts.  Here we need to get all the albums
        //  that have media that are podcasts.
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String [] projection = new String [] {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ALBUM_ID,
        };
        String selection = MediaStore.Audio.Media.IS_PODCAST + " = 1";
        String sort = MediaStore.Audio.Media.ALBUM_ID + " ASC";
        Cursor podcasts = mContext.getContentResolver().query(uri, projection, selection, null, sort);
        Log.d(TAG, "Found " + podcasts.getCount() + " podcast files");

        // Once the query is complete, query the db to get all the albums that were found in the
        //  previous query;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        Integer lastId = null;
        podcasts.moveToFirst();
        while(!podcasts.isAfterLast()) {
            int thisId = podcasts.getInt(1);
            if(thisId != lastId) {
                ids.add(thisId);
                lastId = thisId;
            }
            podcasts.moveToNext();
        }
        Log.d(TAG, "Found " + ids.size() + " Albums with podcasts");

        // Return the resulting cursor after it is done loading
        projection = new String[] {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST
        };
        StringBuilder whereBuilder = new StringBuilder();
        whereBuilder.append(MediaStore.Audio.Albums.ALBUM_ID);
        whereBuilder.append(" IN (");
        if(ids.size() == 0) {
            whereBuilder.append("null)");
        }
        else {
            whereBuilder.append("?");
            for (int i = 1; i < ids.size(); i++) {
                whereBuilder.append(", ?");
            }
            whereBuilder.append(")");
        }
        String [] selectionArgs = new String[ids.size()];
        for(int i = 0; i < ids.size(); i++) {
            selectionArgs[i] = ids.get(i).toString();
        }
        sort = MediaStore.Audio.Albums.ALBUM + " ASC";
        return mContext.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                projection, whereBuilder.toString(), selectionArgs, sort);
    }
}
