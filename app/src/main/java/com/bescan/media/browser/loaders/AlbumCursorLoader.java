package com.bescan.media.browser.loaders;

import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

import com.bescan.media.browser.adapters.MediaGroupType;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Cursor Loader that returns a Cursor of albums which do not contain audio books
 * Created by banderson on 1/15/15.
 */
public class AlbumCursorLoader extends CursorLoader {

    public AlbumCursorLoader(Context context) {
        super(context);
    }

    private static final String [] rejectedGenres =
            {"Audiobook"};


    private static final String [] genreProjection = {
            MediaStore.Audio.Genres._ID,
            MediaStore.Audio.Genres.NAME
    };
    private static final String [] songProjection = {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.ALBUM_ID,
            MediaStore.Audio.Media.TITLE
    };
    private static final String songSelection =
            MediaStore.Audio.Media.IS_MUSIC + " > 0";

    private static final String [] albumProjection = {
            MediaStore.Audio.Albums._ID,
            MediaGroupType.subtitleMap.get(MediaGroupType.ALBUM),
            MediaGroupType.titleMap.get(MediaGroupType.ALBUM),
            MediaStore.Audio.Albums.ALBUM_ART
    };

    private static String makePlaceholders(int len) {
        StringBuilder sb = new StringBuilder(len * 2 - 1);
        sb.append("?");
        for(int i = 1; i < len; i++) {
            sb.append(",?");
        }
        return sb.toString();
    }


    private static final String TAG = "AlbCursorLoader";
    @Override
    public Cursor loadInBackground() {
        // Get a list of songs and their album ID
        ArrayList<String> album_ids = new ArrayList<String>();
        ArrayList<String> rejectedGenresList = new
                ArrayList<String>(Arrays.asList(rejectedGenres));

        // Get a list of all the songs
        Cursor cursor = getContext().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                songProjection,
                songSelection,
                null,
                null
        );

        while(cursor.moveToNext()) {
            String title = cursor.getString(2);
            Log.i(TAG, "Checking genres for " + title);
            int songId = cursor.getInt(0);
            int albumId = cursor.getInt(1);
            Cursor genreCursor = getContext().getContentResolver().query(
                    MediaStore.Audio.Genres.getContentUriForAudioId(
                            "external", songId),
                    genreProjection,
                    null, null, null
            );

            boolean reject = false;
            while(genreCursor.moveToNext()) {
                String genre = genreCursor.getString(1);
                if(rejectedGenresList.contains(genre)) {
                    reject = true;
                    break;
                }
            }
            genreCursor.close();

            if(!reject) {
                if(!album_ids.contains("" + albumId)){
                    album_ids.add("" + albumId);
                }
            }
        }
        cursor.close();

        String[] ids = new String[album_ids.size()];
        album_ids.toArray(ids);
        String selection = MediaStore.Audio.Albums._ID  + " IN (" +
                makePlaceholders(ids.length) + ")";

        // We now have a list of album IDs that should appear in the Albums list
        return getContext().getContentResolver().query(
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                albumProjection,
                selection, ids, null
        );
    }
}
