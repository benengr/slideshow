package com.bescan.media.browser.loaders;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.bescan.media.SongInfo;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creates a cursor for songs giving the ability to
 * reject genres
 * Created by banderson on 1/15/15.
 */
public class SongInfoLoader extends AsyncTaskLoader<ArrayList<SongInfo>> {
    public SongInfoLoader(Context context, String selection, String[] selectionArgs, String sortOrder) {
        super(context);
        mSelectionArgs = selectionArgs;
        mSongSelection = selection;
        mSortOrder = sortOrder;
    }

    private static final String [] rejectedGenres =
            {"Audiobook"};


    private static final String [] genreProjection = {
            MediaStore.Audio.Genres._ID,
            MediaStore.Audio.Genres.NAME
    };
    private static final int SONG_ID_COL = 0;
    private static final int SONG_TITLE_COL = 1;
    private static final int SONG_ARTIST_COL = 2;
    private static final int SONG_ALBUM_COL = 3;
    private static final int SONG_DURATION_COL = 4;
    private static final int SONG_FILENAME_COL = 5;
    private static final int SONG_TRACKNUMBER_COL = 6;
    private static final String [] songProjection = {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.TRACK
    };
    private String mSongSelection;
    private String[] mSelectionArgs;
    private String mSortOrder;

    private static final String TAG = "AlbCursorLoader";
    @Override
    public ArrayList<SongInfo> loadInBackground() {
        // Get a list of songs and their album ID
        ArrayList<SongInfo> items = new ArrayList<SongInfo>();
        ArrayList<String> rejectedGenresList = new
                ArrayList<String>(Arrays.asList(rejectedGenres));

        // Get a list of all the songs
        Cursor cursor = getContext().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                songProjection,
                mSongSelection,
                mSelectionArgs,
                mSortOrder
        );

        while(cursor.moveToNext()) {
            String title = cursor.getString(SONG_TITLE_COL);
            Log.i(TAG, "Checking genres for " + title);
            int songId = cursor.getInt(SONG_ID_COL);
            Cursor genreCursor = getContext().getContentResolver().query(
                    MediaStore.Audio.Genres.getContentUriForAudioId(
                            "external", songId),
                    genreProjection,
                    null, null, null
            );

            boolean reject = false;
            while(genreCursor.moveToNext()) {
                String genre = genreCursor.getString(1);
                if(rejectedGenresList.contains(genre)) {
                    reject = true;
                    break;
                }
            }

            if(!reject) {
                SongInfo i = new SongInfo();
                i.setArtist(cursor.getString(SONG_ARTIST_COL));
                i.setTrackName(cursor.getString(SONG_TITLE_COL));
                i.setAlbum(cursor.getString(SONG_ALBUM_COL));
                i.setDuration(cursor.getInt(SONG_DURATION_COL));
                i.setFilename(cursor.getString(SONG_FILENAME_COL));
                i.setTrackNumber(cursor.getInt(SONG_TRACKNUMBER_COL));
                items.add(i);
            }
        }
        return items;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
