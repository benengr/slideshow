package com.bescan.media.browser;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.bescan.media.SongInfo;
import com.bescan.media.browser.adapters.SongInfoAdapter;
import com.bescan.media.browser.loaders.SongInfoLoader;
import com.bescan.mediaslideshow.R;

import com.bescan.utilities.IdManager;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the
 * interface.
 */
public class SongListFragment extends Fragment
        implements AbsListView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<ArrayList<SongInfo>>{
    private static final String TAG = "SongList";
    private static final String ARG_SELECTION = "SelectionKey";
    private static final String ARG_SELECTIONARGS = "SelectionArguments";
    private static final String ARG_SORTORDER = "SortOrder";
    private static final String ARG_URI = "URIARG";
    private int mLoaderId;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private SongInfoAdapter mAdapter;

    /**
     * The default newInstance function will create a song list fragment that
     * @return The newly created fragment
     */
    public static SongListFragment newInstance() {
        return newInstance(null, null, null);
    }
    public static SongListFragment newInstance(String selection, String[] selectionArgs, String sortOrder) {
        return newInstance(null, selection, selectionArgs, sortOrder);
    }
    public static SongListFragment newInstance(Uri uri, String selection, String[] selectionArgs, String sortOrder) {
        SongListFragment f =  new SongListFragment();

        Bundle args = new Bundle();
        args.putString(ARG_SELECTION, selection);
        args.putStringArray(ARG_SELECTIONARGS, selectionArgs);
        if(sortOrder != null) {
            args.putString(ARG_SORTORDER, sortOrder);
        } else {
            args.putString(ARG_SORTORDER, MediaStore.Audio.Media.TITLE + " ASC");
        }

        if(uri == null) {
            args.putParcelable(ARG_URI, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        } else {
            args.putParcelable(ARG_URI, uri);
        }
        f.setArguments(args);

        return f;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SongListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new SongInfoAdapter(getActivity());

        StringBuilder builder = new StringBuilder();
        builder.append(getArguments().getString(ARG_SELECTION));
        String[] args = getArguments().getStringArray(ARG_SELECTIONARGS);
        if (args != null) {
            for (String arg : args) {
                builder.append(arg);
            }
        }
        builder.append(getArguments().getString(ARG_SORTORDER));

        mLoaderId = IdManager.getInstance(this.getClass()).getId(builder.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_songlistfragment, container, false);

        // Set the adapter
        /*
      The fragment's ListView/GridView.
     */
        AbsListView mListView = (AbsListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        getLoaderManager().initLoader(mLoaderId, null, this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public Loader<ArrayList<SongInfo>> onCreateLoader(int i, Bundle bundle) {
        Log.d(TAG, "onCreateLoader()");

        String selection = getArguments().getString(ARG_SELECTION);
        String [] selectionArgs = getArguments().getStringArray(ARG_SELECTIONARGS);
        String sort = getArguments().getString(ARG_SORTORDER);
        return new SongInfoLoader(getActivity(), selection, selectionArgs, sort);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<SongInfo>> cursorLoader, ArrayList<SongInfo> cursor) {
        Log.d(TAG, "onLoadFinished, found " + cursor.size() + " songs");
        mAdapter.swapList(cursor);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<SongInfo>> cursorLoader) {
        Log.d(TAG, "onLoaderReset()");
        mAdapter.swapList(null);
    }
}
