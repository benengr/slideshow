package com.bescan.media.browser;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.app.Fragment;
import android.widget.ListView;
import android.widget.TextView;

import com.bescan.media.browser.adapters.GroupedMediaItemAdapter;
import com.bescan.media.browser.adapters.MediaGroupType;
import com.bescan.media.browser.loaders.PodcastLoader;
import com.bescan.media.browser.loaders.AlbumCursorLoader;
import com.bescan.mediaslideshow.R;
import com.bescan.utilities.DispalyUtils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * Use the {@link SongsByGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SongsByGroupFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener,
        FragmentManager.OnBackStackChangedListener{
    private String TAG = "";
    private static final String TAG_BASE = "SongsByGroupF";
    private static final String ARG_GROUPTYPE = "grouptype";
    private MediaGroupType mType;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AlbumGridFragment.
     */
    public static SongsByGroupFragment newInstance(MediaGroupType t) {
        SongsByGroupFragment f = new SongsByGroupFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_GROUPTYPE, t.ordinal());
        f.setArguments(args);

        return f;
    }

    public SongsByGroupFragment() {

    }

    private boolean useGrid = false;
    private GroupedMediaItemAdapter mAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        double diag = DispalyUtils.getDiagnalSize(dm);

        if(diag >= DispalyUtils.ScreenType.TABLET.getSize()) {
            useGrid = true;
        }

        if (getArguments().get(ARG_GROUPTYPE) != null) {
            mType = MediaGroupType.values()[getArguments().getInt(ARG_GROUPTYPE)];
        }

        TAG = TAG_BASE + "(" + mType.toString() + ")";
        Log.d(TAG, "OnCreate Finished");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        AbsListView list;
        if(useGrid) {
            // Inflate the view
            list = (GridView)inflater.inflate(R.layout.fragment_album_grid, container, false);
        } else {
            list = new ListView(getActivity());
        }
        mAdapter = new GroupedMediaItemAdapter(getActivity(), mType, useGrid);
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(this);

        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        getLoaderManager().initLoader(mAdapter.getId(), null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.d(TAG, "onCreateLoader()");

        if(mType == MediaGroupType.PODCAST) {
            return new PodcastLoader(getActivity());
        } else if (mType == MediaGroupType.ALBUM) {
            return new AlbumCursorLoader(getActivity());
        }

        ArrayList<String> projection = new ArrayList<String>();
        projection.add("_id");

        String titleColumn = MediaGroupType.titleMap.get(mType);
        Uri uri = MediaGroupType.uriMap.get(mType);
        String subTitleColumn = MediaGroupType.subtitleMap.get(mType);

        projection.add(titleColumn);
        if(subTitleColumn != null) {
            projection.add(subTitleColumn);
        }

        if(mType == MediaGroupType.ALBUM) {
            projection.add(MediaStore.Audio.Albums.ALBUM_ART);
        }

        String [] temp = new String[projection.size()];
        temp = projection.toArray(temp);
        return new CursorLoader(getActivity(), uri, temp, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.i(TAG, "Finished Loading Cursor, found " + cursor.getCount() + " items");
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        Log.d(TAG, "onLoaderReset()");
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Fragment newFrag = null;
        switch(mType) {
            case ALBUM: {
                String selection = MediaStore.Audio.Media.ALBUM + "= ?";
                TextView t = (TextView) view.findViewById(R.id.title);
                String[] args = new String[1];
                args[0] = t.getText().toString();
                String sort = MediaStore.Audio.Media.TRACK + " ASC";
                newFrag = SongListFragment.newInstance(selection, args, sort);
                break;
            }
            case ARTIST:
                newFrag = SongsByGroupFragment.newInstance(MediaGroupType.ALBUM);
                break;
            case GENRE: {
                Uri uri = MediaStore.Audio.Genres.Members.getContentUri("external", (Integer) view.getTag());
                String sort = MediaStore.Audio.Media.TITLE + " ASC";
                newFrag = SongListFragment.newInstance(uri, null, null, sort);
                break;
            }
            case PLAYLIST: {
                Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", (Integer) view.getTag());
                String sort = MediaStore.Audio.Media.TITLE + " ASC";
                newFrag = SongListFragment.newInstance(uri, null, null, sort);
                break;
            }
            case AUDIOBOOK:
                newFrag = SongListFragment.newInstance();
                break;
            case PODCAST:
                newFrag = SongListFragment.newInstance();
                break;
        }

        // Now that we have a fragment, load it
        if(newFrag != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.browser_container, newFrag);
            ft.addToBackStack(null);
            ft.commit();
            getActivity().getSupportFragmentManager().addOnBackStackChangedListener(this);
        }
    }


    @Override
    public void onBackStackChanged() {
        if(mAdapter != null) {
            if(isAdded()) {
                getLoaderManager().initLoader(mAdapter.getId(), null, this);
            }
        }
    }
}
