package com.bescan.media.browser.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.apache.http.auth.AUTH;

/**
 * Created by banderson on 1/16/15.
 */
public class MediaBrowserDatabaseHelper extends SQLiteOpenHelper {

    public MediaBrowserDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // public
    // Song Table
    public class Songs {
        public static final String TABLE_NAME = "songs";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name";
        public static final String ALBUMID_COLUMN = "album_id";
        public static final String TRACKNUMBER_COLUMN = "track_number";
        public static final String ARTWORK_COLUMN = "artwork";
        public static final String DATA_COLUMN = "data";
        public static final String ARTISTID_COLUMN = "artist_id";
        public static final String YEAR_COLUMN = "year";
        public static final String DURATION_COLUMN = "duration_ms";
        public static final String GENRE_COLUMN = "genre_id";
        public static final String RANKING_COLUMN = "ranking";

        public static final String SQL_CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        ALBUMID_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        TRACKNUMBER_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        ARTWORK_COLUMN + TEXT_TYPE + COMMA_SEP +
                        DATA_COLUMN + TEXT_TYPE + COMMA_SEP +
                        ARTISTID_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        YEAR_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        DURATION_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        GENRE_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        RANKING_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }
    public class Albums {
        public static final String TABLE_NAME = "albums";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name";
        public static final String ARTIST_COLUMN = "artist_id";
        public static final String ARTWORK_COLUMN = "artwork";
        public static final String YEAR_COLUMN = "year";
        public static final String GENRE_COLUMN = "genre";

        public static final String SQL_CREATE_ALBUM_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        ARTIST_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        GENRE_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        ARTWORK_COLUMN + TEXT_TYPE + COMMA_SEP +
                        YEAR_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }
    public class Artists {
        public static final String TABLE_NAME = "artists";
        public static final String ID_COLUMN = "_id";
        public static final String FIRST_NAME_COLUMN = "first_name";
        public static final String LAST_NAME_COLUMN = "last_name";
        public static final String URL_COLUMN = "url";
        public static final String ARTWORK_COLUMN = "artwork";

        public static final String CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        FIRST_NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        LAST_NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        URL_COLUMN + TEXT_TYPE + COMMA_SEP +
                        ARTWORK_COLUMN + TEXT_TYPE +
                TABLE_END;
    }
    public class Bands {

        public static final String TABLE_NAME = "bands";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name_id";
        public static final String ARTIST_COLUMN = "artist_id";

        public static final String SQL_CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        NAME_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        ARTIST_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }
    public class Playlists {
        public static final String TABLE_NAME = "playlists";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name";

        public static final String SQL_CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                    ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                    NAME_COLUMN + TEXT_TYPE +
                TABLE_END;
    }
    public class PlaylistMap {
        public static final String TABLE_NAME = "playlist_map";
        public static final String ID_COLUMN = "_id";
        public static final String PLAYLIST_COLUMN = "playlist";
        public static final String SONG_COLUMN = "song";

        public static final String SQL_CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        PLAYLIST_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        SONG_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }
    public class AudioBook {
        public static final String TABLE_NAME = "audiobooks";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name";
        public static final String AUTHOR_COLUMN = "author";
        public static final String NARRATOR_COLUMN = "performer";
        public static final String GENRE_COLUMN = "genre";
        public static final String ARTWORK_COLUMN = "artwork";
        public static final String YEAR_COLUMN = "year";
        public static final String DURATION_COLUMN = "duration";
        public static final String RANKING_COLUMN = "ranking";

        public static final String CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        AUTHOR_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        NARRATOR_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        GENRE_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        ARTWORK_COLUMN + TEXT_TYPE + COMMA_SEP +
                        YEAR_COLUMN + INTEGER_TYPE + COMMA_SEP +
                        DURATION_COLUMN + INTEGER_TYPE +COMMA_SEP +
                        RANKING_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }
    public class Genre {
        public static final String TABLE_NAME = "genres";
        public static final String ID_COLUMN = "_id";
        public static final String NAME_COLUMN = "name";
        public static final String RANKING_COLUMN = "ranking";

        public static final String CREATE_TABLE =
                TABLE_START + TABLE_NAME + "(" +
                        ID_COLUMN + PRIMARY_KEY_TYPE + COMMA_SEP +
                        NAME_COLUMN + TEXT_TYPE + COMMA_SEP +
                        RANKING_COLUMN + INTEGER_TYPE +
                TABLE_END;
    }

    // Private helpers
    private static final String COMMA_SEP = ", ";
    private static final String PRIMARY_KEY_TYPE = " INT PRIMARY KEY NOT NULL";
    private static final String INTEGER_TYPE = " INT";
    private static final String TEXT_TYPE = " TEXT";
    private static final String TABLE_START = "CREATE TABLE ";
    private static final String TABLE_END = ");";

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
