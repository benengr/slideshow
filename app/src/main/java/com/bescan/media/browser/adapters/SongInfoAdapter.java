package com.bescan.media.browser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bescan.media.SongInfo;
import com.bescan.mediaslideshow.R;

import java.util.ArrayList;

/**
 * Displays a list of songs
 *
 * Created by banderson on 1/15/15.
 */
public class SongInfoAdapter extends BaseAdapter {

    private ArrayList<SongInfo> data;
    private Context mContext;
    public SongInfoAdapter(Context c) {
        super();
        mContext = c;
        data = new ArrayList<SongInfo>();
    }

    public void swapList(ArrayList<SongInfo> newList) {
        if(newList == null)
            data.clear();
        else
            data = newList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return data.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View v = view;
        if(v == null)
            v = inflater.inflate(R.layout.list_item_view, viewGroup, false);

        TextView text = (TextView)v.findViewById(R.id.firstLine);
        text.setText(data.get(i).getTrackName());

        text = (TextView)v.findViewById(R.id.secondLine);
        text.setText(data.get(i).getArtist());

        return v;
    }
}
