package com.bescan.media.browser;

import android.app.Activity;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bescan.media.SongInfo;
import com.bescan.media.browser.adapters.MediaGroupType;
import com.bescan.mediaslideshow.R;

import java.util.Collection;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.bescan.media.browser.MediaBrowserFragment.OnMediaSelectedListener} interface
 * to handle interaction events.
 * Use the {@link MediaBrowserFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class MediaBrowserFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_START_VIEW = "starting_view";
    public static final String TAG = "MediaBrowserFragment";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param startingView Parameter 1.
     * @return A new instance of fragment MediaBrowserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MediaBrowserFragment newInstance(String startingView) {
        MediaBrowserFragment fragment = new MediaBrowserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_START_VIEW, startingView);
        fragment.setArguments(args);
        return fragment;
    }
    public MediaBrowserFragment() {
        // Required empty public constructor
    }

    @Override
    @SuppressWarnings("unused")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString(ARG_START_VIEW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_media_browser, container, false);
        ViewPager mPager = (ViewPager) v.findViewById(R.id.pager);
        MediaBrowserPagerAdapter mAdapter = new MediaBrowserPagerAdapter(getChildFragmentManager());
        mPager.setAdapter(mAdapter);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class MediaBrowserPagerAdapter extends FragmentStatePagerAdapter{
        private final String [] pageTitles = {
                "Albums",
                "Artists",
                "Songs",
                "Playlists",
                "Genres",
                "Podcasts"
        };
        private final int ALBUM_INDEX = 0;
        private final int ARTIST_INDEX = 1;
        private final int SONG_INDEX = 2;
        private final int PLAYLIST_INDEX = 3;
        private final int GENRE_INDEX = 4;
        private final int PODCAST_INDEX = 5;

        public MediaBrowserPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int i) {
            switch(i) {
                case ALBUM_INDEX:
                    return SongsByGroupFragment.newInstance(
                            MediaGroupType.ALBUM);
                case ARTIST_INDEX:
                    return SongsByGroupFragment.newInstance(MediaGroupType.ARTIST);
                case SONG_INDEX:
                    return SongListFragment.newInstance(
                            MediaStore.Audio.Media.IS_MUSIC + " > 0",
                            null,
                            null
                    );
                case PLAYLIST_INDEX:
                    return SongsByGroupFragment.newInstance(MediaGroupType.PLAYLIST);
                case GENRE_INDEX:
                    return SongsByGroupFragment.newInstance(MediaGroupType.GENRE);
                case PODCAST_INDEX:
                    return SongsByGroupFragment.newInstance(MediaGroupType.PODCAST);
            }
            return null;
        }

        @Override
        public int getCount() {
            return pageTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageTitles[position];
        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMediaSelectedListener {
        @SuppressWarnings("unused")
        public void playNow(Collection<SongInfo> songs);
        @SuppressWarnings("unused")
        public void addToQueue(Collection<SongInfo> songs);
    }

}
