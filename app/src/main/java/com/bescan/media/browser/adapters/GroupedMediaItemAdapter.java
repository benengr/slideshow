package com.bescan.media.browser.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bescan.media.browser.BlankOnErrorListener;
import com.bescan.mediaslideshow.R;
import com.bescan.utilities.IdManager;
import com.nostra13.universalimageloader.core.ImageLoader;


/**
 * This adapter is used to load media items in groups such as songs by album or songs by artist
 * or songs in a playlist.
 *
 * Created by Ben on 10/16/2014.
 */
public class GroupedMediaItemAdapter extends CursorAdapter implements View.OnClickListener {


    private int myId;
    private int layoutId;
    MediaGroupType mType;

    public GroupedMediaItemAdapter(Context context, MediaGroupType t, boolean useGrid) {
        super(context, null, 0 );

        if(useGrid) {
            layoutId = R.layout.grouped_media_item_view;
        } else {
            layoutId = R.layout.grouped_media_item_view_list;
        }
        if(t == null) {
            throw new NullPointerException();
        }
        String hash = t.toString();
        myId = IdManager.getInstance(this.getClass()).getId(hash);
        mType = t;
    }
    public int getId() {
        return myId;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(layoutId, viewGroup, false);
    }

    @Override
    public void bindView(View v, Context context, Cursor cursor) {
        // Check if there is a subtitle
        int index;
        boolean didset = false;
        TextView current = (TextView)v.findViewById(R.id.subtitle);

        v.setBackgroundColor(0x44FFFFFF);

        String subTitleColumn = MediaGroupType.subtitleMap.get(mType);
        if(subTitleColumn != null) {
            index = cursor.getColumnIndex(subTitleColumn);
            if(index > -1) {
                current.setText(cursor.getString(index));
                didset = true;
            }
        }
        if(!didset) {
            current.setText("");
            current.setVisibility(View.INVISIBLE);
        }

        String titleColumn = MediaGroupType.titleMap.get(mType);
        index = cursor.getColumnIndex(titleColumn);
        if(index > -1)
            ((TextView)v.findViewById(R.id.title)).setText(cursor.getString(index));

        // Right now, we are only displaying an image for the album view
        ImageView img = (ImageView)v.findViewById(R.id.artwork);
        img.setBackgroundColor(0x88000000);
        if(mType == MediaGroupType.ALBUM) {
            String u = null;
            img.setVisibility(View.VISIBLE);
            img.setEnabled(true);
            index = cursor.getColumnIndex(MediaGroupType.imageUriMap.get(mType));
            if(index > -1) {
                String path = cursor.getString(index);
                if(path != null) {
                    u = "file://" + path;
                }
                if(u != null) {
                    ImageLoader.getInstance().displayImage(u, img, null, new BlankOnErrorListener());

                } else {
                    img.setImageDrawable(null);
                }

            }
        } else {
            img.setVisibility(View.INVISIBLE);
            img.setEnabled(false);
        }

        v.setTag(cursor.getInt(0));
    }

    @Override
    public void onClick(View view) {

    }
}
