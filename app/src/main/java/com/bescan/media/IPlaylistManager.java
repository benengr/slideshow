package com.bescan.media;

import java.util.Collection;

/**
 * Interface that specifies functionality of an object which manages a playlist.  The playlist is a
 * list of Media items which will be played in a certain order.  Note that this interface does not
 * specify how to start and stop playback of the playlist, just setting the properties and the
 * contents of the list.
 *
 * Created by banderson on 10/15/14.
 */
public interface IPlaylistManager {
    /**
     * Adds a song to the end of the playlist
     * @param info The song to be added
     */
    void addSong(SongInfo info);

    /**
     * Add a collection of songs to the playlist.
     * @param info The collection of songs to be added
     */
    void addSongs(Collection<SongInfo> info);

    /**
     * Removes all songs from the playlist
     */
    void removeAllSongs();

    /**
     * Remove the song represented by the SongInfo object.
     * @param info The song to be removed from the list.  Note, this needs to be the same object
     *             that was put into the list with either the {@link #addSong(SongInfo)} or the
     *             {@link #addSongs(java.util.Collection)} methods, not just an SongInfo object
     *             with the same contents.
     */
    void removeSong(SongInfo info);

    /**
     * Removes the item from the specified index
     * @param index The index of the item to be removed.
     */
    void removeSongAt(int index);

    /**
     * Get the current number of items in the playlist
     * @return The current number of items in the playlist
     */
    int getCount();

    /**
     * Get the {@link com.bescan.media.SongInfo} object at the specified index
     * @param index The index of the item to get
     * @return The item at the specified index
     */
    SongInfo getSongAt(int index);

    /**
     * Get the song that is currently playing from the playlist.  Note, by currently playing this
     * means the song that will be played if the player is stopped or resumed if it is paused.
     *
     * @return The currently playing song.  Will be null if there is no current song.
     */
    SongInfo getCurrentSong();

    /**
     *
     * @param doShuffle
     */
    void shuffle(boolean doShuffle);
    void setRepeatTrack(boolean doRepeat);
    void setRepeatList(boolean doRepeat);
}
