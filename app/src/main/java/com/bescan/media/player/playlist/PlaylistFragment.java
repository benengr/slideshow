package com.bescan.media.player.playlist;

import android.support.v4.app.ListFragment;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.content.ServiceConnection;

import com.bescan.media.player.service.AndroidMediaPlayer;


/**
 * This class interacts with the MediaPlayerService and displays the playlist
 *
 * Created by banderson on 10/1/14.
 */
public class PlaylistFragment extends ListFragment {
    AndroidMediaPlayer service;
    boolean mBound = false;
    private final Object lock = new Object();
    public static final String TAG = "PlaylistFragment";

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        setListAdapter(new PlaylistAdapter(getActivity()));

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        synchronized (lock) {
            if (service != null) {
                service.play(position);
            }
        }
    }

    public PlaylistFragment() {
        super();
    }

    @Override
    public void onStop() {
        super.onStop();

        synchronized (lock) {
            if(mBound) {
                getActivity().unbindService(mConnection);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        v.setBackgroundColor(0xFFFFAAAA);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent i = new Intent(getActivity(), AndroidMediaPlayer.class);
        getActivity().bindService(i, mConnection, 0);
    }

    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (lock) {
                AndroidMediaPlayer.LocalBinder b = (AndroidMediaPlayer.LocalBinder)iBinder;
                service = b.getService();
                mBound = true;
                ((PlaylistAdapter)getListAdapter()).setService(service);

            }
            Log.v(TAG, "Bound to media player service");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            synchronized (lock) {
                service = null;
                mBound = false;
                ((PlaylistAdapter)getListAdapter()).setService(null);
            }
            Log.v(TAG, "Unbound from service");
        }
    };
}
