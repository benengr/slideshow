package com.bescan.media.player.service;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.widget.MediaController;

public class MusicIntentReceiver extends android.content.BroadcastReceiver {
    public MusicIntentReceiver(MediaController.MediaPlayerControl player) {
        mPlayer = player;
    }
    MediaController.MediaPlayerControl mPlayer;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
            if(mPlayer != null) {
                mPlayer.pause();
            }
        }
    }
}
