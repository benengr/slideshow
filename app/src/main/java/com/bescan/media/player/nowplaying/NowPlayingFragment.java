package com.bescan.media.player.nowplaying;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bescan.media.SongInfo;
import com.bescan.media.player.service.AndroidMediaPlayer;
import com.bescan.mediaslideshow.R;


import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 *
 */
public class NowPlayingFragment extends Fragment
        implements AndroidMediaPlayer.OnNowPlayingChangedListener,
        AndroidMediaPlayer.OnStateChangedListener {
    private SeekBar p;
    private View rootView = null;
    private static final String TAG = "NowPlayingFragment";
    private TextView mRemaining;
    private boolean mDragging = false;
    private final Object lock = new Object();
    private Timer viewUpdateTimer = null;
    private final Object timerLock = new Object();
    private AndroidMediaPlayer mPlayer;

    public NowPlayingFragment() {
        // Required empty public constructor
    }



    private void startViewUpdateTimer() {
        synchronized (timerLock) {
            if(viewUpdateTimer == null) {
                viewUpdateTimer = new Timer();
                viewUpdateTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setProgress();
                            }
                        });
                    }
                }, 0, 1000);
            }
        }
    }

    private void stopViewUpdateTimer() {
        synchronized (timerLock) {
            if(viewUpdateTimer != null) {
                viewUpdateTimer.cancel();
                viewUpdateTimer = null;
            }
        }
    }
    @Override
    public void onPause() {
        super.onPause();

        synchronized (lock) {
            if(mPlayer != null) {
                mPlayer.removeOnNowPlayingListener(this);
                getActivity().unbindService(mConnection);
                mPlayer.removeStateChangedListener(this);
                mPlayer.removeOnNowPlayingListener(this);
                mPlayer = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent i = new Intent(getActivity(), AndroidMediaPlayer.class);
        getActivity().bindService(i, mConnection, 0);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_now_playing, container, false);
        // Bind UI Elements to functions
        p = (SeekBar)rootView.findViewById(R.id.now_playing_seekbar);
        p.setOnSeekBarChangeListener(new SeekBarChangeHandler());
        p.setIndeterminate(false);
        p.setIndeterminateDrawable(null);
        p.setMax(1000);
        mRemaining = (TextView)rootView.findViewById(R.id.now_playing_remaining);
        return rootView;
    }

    @Override
    public void onStateChanged(AndroidMediaPlayer.PlayerState newState) {
        if(newState == AndroidMediaPlayer.PlayerState.PLAYING) {
            startViewUpdateTimer();
        } else {
            stopViewUpdateTimer();
        }
    }

    private class SeekBarChangeHandler implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromuser) {
            if(!fromuser)
                return;
            if(mPlayer == null)
                return;

            long duration = mPlayer.getDuration();
            long new_position = (duration * progress) / 1000L;
            mPlayer.seekTo((int)new_position);

            UpdateTimeRemaining((int)duration, (int)new_position);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mDragging = true;
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mDragging = false;
            setProgress();
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNowPlayingChanged(SongInfo currentSong) {
        if(rootView == null)
            return;


        if(currentSong != null) {
            TextView t = (TextView)rootView.findViewById(R.id.nowPlaying_Album);
            t.setText(currentSong.getAlbum());

            t = (TextView)rootView.findViewById(R.id.nowPlaying_Artist);
            t.setText(currentSong.getArtist());

            t = (TextView)rootView.findViewById(R.id.nowPlaying_Title);
            t.setText(currentSong.getTrackName());

            startViewUpdateTimer();

        } else {
            TextView t = (TextView)rootView.findViewById(R.id.nowPlaying_Album);
            t.setText("");

            t = (TextView)rootView.findViewById(R.id.nowPlaying_Artist);
            t.setText("");

            t = (TextView)rootView.findViewById(R.id.nowPlaying_Title);
            t.setText("");

            stopViewUpdateTimer();
        }

    }

    private String getTimeFromMs(int ms) {
        int seconds =  (ms/1000) % 60;

        ms = ms - (seconds * 1000);

        int minutes = (ms / (1000 * 60)) % 60;
        ms = ms - minutes * 1000 * 60;

        int hours =  ms / (1000 * 60 * 60);

        if(hours > 0) {
            return String.format("%d:%02d:%02d", hours, minutes, seconds);
        } else {
            return String.format("%02d:%02d", minutes, seconds);
        }
    }

    private void setProgress() {

        AndroidMediaPlayer player = mPlayer;

        if(player == null || mDragging) {
            return;
        }

        int position = player.getCurrentPosition();
        int duration = player.getDuration();

        if(p != null) {
            if(duration > 0) {
                long pos = 1000L * position / duration;
                p.setProgress((int)pos);
            }

        }
        UpdateTimeRemaining(duration, position);
    }
    private void UpdateTimeRemaining(int total, int current) {
        if(mRemaining == null) return;

        final String ellapsed = getTimeFromMs(current);
        final String strTotal = getTimeFromMs(total);
        mRemaining.setText(ellapsed + "/" + strTotal);
    }


    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (lock) {
                AndroidMediaPlayer.LocalBinder b = (AndroidMediaPlayer.LocalBinder)iBinder;
                mPlayer = b.getService();

                Log.v(TAG, "Bound to service with player hash: " + mPlayer.hashCode());
                mPlayer.addOnNowPlayingListener(NowPlayingFragment.this);

                SongInfo i = mPlayer.getCurrentSong();
                onNowPlayingChanged(i);
            }
            Log.v(TAG, "Bound to media player service");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            synchronized (lock) {

                try {
                    if(mPlayer != null) {
                        mPlayer.removeOnNowPlayingListener(NowPlayingFragment.this);
                        mPlayer.removeStateChangedListener(NowPlayingFragment.this);
                    }
                    mPlayer = null;

                } catch(Exception ex) {
                    Log.w(TAG, ex.getMessage());
                }
            }
            Log.v(TAG, "Unbound from service");
        }
    };
}
