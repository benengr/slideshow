package com.bescan.media.player.controls;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.app.Fragment;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bescan.media.SongInfo;
import com.bescan.media.player.service.AndroidMediaPlayer;
import com.bescan.mediaslideshow.R;

import java.util.Formatter;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A Fragment that controls the operations of the
 * {@link com.bescan.media.player.service.AndroidMediaPlayer}.  This binds to the service
 * and allows the user to interact with the media player.
 *
 * Use the {@link PlayerControlFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class PlayerControlFragment extends Fragment
        implements
        AndroidMediaPlayer.OnNowPlayingChangedListener,
        AndroidMediaPlayer.OnStateChangedListener {

    private final Object lock = new Object();
    private AndroidMediaPlayer mPlayer = null;
    private static final String TAG = "PlayerControlFragment";
    private View mRoot;
    private ImageButton mPauseButton;
    private ImageButton mRewButton;
    private ImageButton mFfwdButton;
    private TextView mCurrentTime, mEndTime;
    private boolean mDragging;
    private SeekBar mProgress;
    private Timer viewUpdateTimer;
    private final Object timerLock = new Object();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PlayerControlFragment.
     */
    public static PlayerControlFragment newInstance() {
        return new PlayerControlFragment();
    }

    public PlayerControlFragment() {
        // Required empty public constructor
    }

    private void startViewUpdateTimer() {
        synchronized (timerLock) {
            if(viewUpdateTimer == null) {
                viewUpdateTimer = new Timer();
                viewUpdateTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setProgress();
                            }
                        });
                    }
                }, 0, 1000);
            }
        }
    }

    private void stopViewUpdateTimer() {
        synchronized (timerLock) {
            if(viewUpdateTimer != null) {
                viewUpdateTimer.cancel();
                viewUpdateTimer = null;
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRoot = inflater.inflate(R.layout.fragment_player_control, container, false);
        mPauseButton = (ImageButton)mRoot.findViewById(R.id.mediacontroller_pause);
        mPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doPauseResume();
            }
        });
        mRewButton = (ImageButton)mRoot.findViewById(R.id.mediacontroller_rew);
        mFfwdButton = (ImageButton)mRoot.findViewById(R.id.mediacontroller_ffwd);
        ImageButton mPrevButton = (ImageButton) mRoot.findViewById(R.id.mediacontroller_prev);
        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.prev();
                }

            }
        });
        ImageButton mNextButton = (ImageButton) mRoot.findViewById(R.id.mediacontroller_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.next();
                }
            }
        });

        mCurrentTime = (TextView)mRoot.findViewById(R.id.mediacontroller_currentTime);
        mEndTime = (TextView)mRoot.findViewById(R.id.mediacontroller_duration);
        mProgress = (SeekBar)mRoot.findViewById(R.id.mediacontroller_progress);
        mProgress.setMax(1000);
        mProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromuser) {
                if(!fromuser)
                    return;
                if(mPlayer == null)
                    return;

                long duration = mPlayer.getDuration();
                long new_position = (duration * progress) / 1000L;
                mPlayer.seekTo((int)new_position);
                if(mCurrentTime != null) {
                    mCurrentTime.setText(stringForTime((int)new_position));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mDragging = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mDragging = false;
                setProgress();
                updatePausePlay();
            }
        });
        disableUnsupportedButtons();

        return mRoot;
    }

    private void attachPlayer() {
        updatePausePlay();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Intent i = new Intent(getActivity(), AndroidMediaPlayer.class);
        getActivity().bindService(i, mConnection, 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if(mPlayer != null){
            getActivity().unbindService(mConnection);
            mPlayer.removeOnNowPlayingListener(this);
            mPlayer.removeStateChangedListener(this);
            mPlayer = null;
        }
    }

    private void updatePausePlay() {
        if(mRoot == null)
            return;

        synchronized (lock) {
            if(mPlayer != null) {
                if(mPlayer.isPlaying()) {
                    mPauseButton.setImageResource(android.R.drawable.ic_media_pause);
                } else {
                    mPauseButton.setImageResource(android.R.drawable.ic_media_play);
                }
            }
        }
    }

    private void disableUnsupportedButtons() {

        AndroidMediaPlayer p = mPlayer;
        if(p == null) return;

        try {
            if(mPauseButton != null && !p.canPause()) {
                mPauseButton.setEnabled(false);
            }
            if(mRewButton != null && !p.canSeekBackward()) {
                mRewButton.setEnabled(false);
            }
            if(mFfwdButton != null && !p.canSeekForward()) {
                mFfwdButton.setEnabled(false);
            }
        } catch (Exception e) {
            // Ignore any issues;
        }
    }

    private int setProgress () {
        synchronized (lock) {

            AndroidMediaPlayer p = mPlayer;

            if(p == null || mDragging) {
                return 0;
            }

            int position = p.getCurrentPosition();
            int duration = p.getDuration();

            if(mProgress != null) {
                if(duration > 0) {
                    long pos = 1000L * position / duration;
                    mProgress.setProgress((int)pos);
                }

            }

            if(mEndTime != null) {
                mEndTime.setText(stringForTime(duration));
            }
            if(mCurrentTime != null) {
                mCurrentTime.setText(stringForTime(position));
            }

            return position;
        }
    }

    private String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        StringBuilder mFormatBuilder = new StringBuilder();
        mFormatBuilder.setLength(0);
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }
    private void doPauseResume() {
        synchronized (lock) {
            if (mPlayer == null) return;

            if (mPlayer.isPlaying()) {
                mPlayer.pause();
            } else {
                mPlayer.start();
            }
            updatePausePlay();
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (lock) {
                AndroidMediaPlayer.LocalBinder b = (AndroidMediaPlayer.LocalBinder)iBinder;
                mPlayer = b.getService();
                mPlayer.addOnNowPlayingListener(PlayerControlFragment.this);
                mPlayer.addOnStateChangedListener(PlayerControlFragment.this);
                attachPlayer();
                disableUnsupportedButtons();
            }
            Log.v(TAG, "Bound to media player service");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            synchronized (lock) {
                mPlayer.removeOnNowPlayingListener(PlayerControlFragment.this);
                mPlayer.removeStateChangedListener(PlayerControlFragment.this);
                mPlayer = null;
            }
            Log.v(TAG, "Unbound from service");
        }
    };

    @Override
    public void onNowPlayingChanged(SongInfo currentSong) {
        if(currentSong != null) {
            startViewUpdateTimer();
        } else {
            stopViewUpdateTimer();
        }
    }

    @Override
    public void onStateChanged(AndroidMediaPlayer.PlayerState newState) {
        if(newState == AndroidMediaPlayer.PlayerState.PLAYING) {
            startViewUpdateTimer();
        } else {
            stopViewUpdateTimer();
        }
    }
}
