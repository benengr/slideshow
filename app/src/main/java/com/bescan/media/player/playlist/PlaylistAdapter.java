package com.bescan.media.player.playlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.bescan.media.player.service.AndroidMediaPlayer;
import com.bescan.mediaslideshow.R;

/**
 *
 * Created by banderson on 10/1/14.
 */
public class PlaylistAdapter extends BaseAdapter implements AndroidMediaPlayer.OnPlaylistChangedListener{
    private AndroidMediaPlayer service;
    private Context context;
    // private static final String TAG = "PlaylistAdapter";
    PlaylistAdapter(Context c) {
        if(c == null) {
            throw new NullPointerException();
        }
        context = c;
    }

    public void setService(AndroidMediaPlayer s) {
        if(service != null) {
            service.removeOnPlaylistChanedListener(this);
        }
        service = s;
        notifyDataSetChanged();
        if(service != null) {
            s.addOnPlaylistChangedListener(this);
        }
    }

    @Override
    public int getCount() {
        if(service != null) {
            return service.getCount();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if(service == null)
            return null;
        return service.getSongAt(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View r = view;
        if(context == null)
            return null;

        // If the Media Player isn't connected let the user know
        if(service == null) {
            LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(inflater == null)
                return null;
            r = inflater.inflate(R.layout.list_item_view, viewGroup);
            TextView v = (TextView) r.findViewById(R.id.firstLine);
            v.setText("Not Connected to Media Player");

            return r;
        }

        // If we need to inflate the view, do it.  Otherwise we are recycling a previous view
        if(r == null) {
            LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            r = inflater.inflate(R.layout.list_item_view, viewGroup, false);
        }

        // Set the Track Name
        TextView v = (TextView) r.findViewById(R.id.firstLine);
        v.setText(service.getSongAt(i).getTrackName());
        v.setTextColor(0xFF000000);

        // Set the Track Title
        v = (TextView) r.findViewById(R.id.secondLine);
        v.setText(service.getSongAt(i).getArtist());
        v.setTextColor(0xFF444444);

        return r;
    }

        @Override
    public void onPlaylistChanged(AndroidMediaPlayer player) {
        notifyDataSetChanged();
    }
}
