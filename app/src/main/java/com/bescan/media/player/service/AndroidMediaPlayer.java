package com.bescan.media.player.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.MediaController;

import com.bescan.media.IPlaylistManager;
import com.bescan.media.SongInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

public class AndroidMediaPlayer extends Service
        implements
        IPlaylistManager,
        MediaController.MediaPlayerControl,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener{

    private static final int NOW_PLAYING_NOTIFICATION_ID = 8675301;

    public AndroidMediaPlayer() {
    }
    private final IBinder mBinder = new LocalBinder();

    // Implementation of IPlaylistManager interface
    @Override
    public void addSong(SongInfo path) {
        playlist.add(path);
        for(OnPlaylistChangedListener l : playlistChangedListeners) {
            l.onPlaylistChanged(this);
        }
    }

    @Override
    public void addSongs(Collection<SongInfo> list) {
        playlist.addAll(list);
        for(OnPlaylistChangedListener l : playlistChangedListeners) {
            l.onPlaylistChanged(this);
        }
    }
    @Override
    public int getCount() {
        return playlist.size();
    }

    @Override
    public void removeAllSongs() {
        synchronized (lockObj) {
            stop();
            currentIndex = 0;
            playlist.clear();
            freeMediaPlayer();
        }
    }

    @Override
    public void removeSong(SongInfo info) {

    }

    @Override
    public void removeSongAt(int index) {

    }

    @Override
    public SongInfo getSongAt(int index) {
        if(index < playlist.size())
            return playlist.get(index);
        return null;
    }

    @Override
    public SongInfo getCurrentSong() {
        if(currentIndex >= playlist.size()) {
            return null;
        }
        return playlist.get(currentIndex);
    }

    @Override
    public void shuffle(boolean doShuffle) {
        synchronized (lockObj) {
            boolean restart = false;
            if(mState != PlayerState.STOPPED) {
                stop();
                restart = true;
            }
            Collections.shuffle(playlist);
            currentIndex = 0;
            if(restart) {
                play();
            }
        }
    }

    @Override
    public void setRepeatTrack(boolean doRepeat) {

    }

    @Override
    public void setRepeatList(boolean doRepeat) {

    }
    // End Implementation of IPlaylistManager Interface

    public class LocalBinder extends Binder {
        public AndroidMediaPlayer getService() {
            return AndroidMediaPlayer.this;
        }
    }

    private MusicIntentReceiver mReceiver;
    @Override
    public void onCreate() {
        super.onCreate();
        am = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        playlist = new ArrayList<SongInfo>();
        mState = PlayerState.STOPPED;

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.AUDIO_BECOMING_NOISY");
        mReceiver = new MusicIntentReceiver(this);
        registerReceiver(mReceiver, filter);
    }
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "onBind() called");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        freeMediaPlayer();
        this.unregisterReceiver(mReceiver);
    }
    @Override
    public void start() {
        play();
    }

    @Override
    public int getDuration() {
        synchronized (lockObj) {
            if(mState != PlayerState.STOPPED)
                return getMediaPlayer().getDuration();
            else return 0;
        }
    }

    @Override
    public boolean isPlaying() {
        synchronized (lockObj) {
            return mState == PlayerState.PLAYING;
        }
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        synchronized (lockObj) {
            if(mState == PlayerState.PLAYING)
                return true;
        }
        return false;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    public interface OnPlaylistChangedListener {
        void onPlaylistChanged(AndroidMediaPlayer player);
    }
    public interface OnNowPlayingChangedListener {
        void onNowPlayingChanged(SongInfo currentSong);
    }
    public interface OnStateChangedListener {
        void onStateChanged(PlayerState newState);
    }

    private ArrayList<OnPlaylistChangedListener> playlistChangedListeners =
            new ArrayList<OnPlaylistChangedListener>();
    private ArrayList<OnNowPlayingChangedListener> nowPlayingChangedListeners =
            new ArrayList<OnNowPlayingChangedListener>();
    private ArrayList<OnStateChangedListener> stateChangedListeners =
            new ArrayList<OnStateChangedListener>();

    public void addOnPlaylistChangedListener(OnPlaylistChangedListener l) {
        if(playlistChangedListeners.contains(l))
            return;
        playlistChangedListeners.add(l);
    }
    public void removeOnPlaylistChanedListener(OnPlaylistChangedListener l) {
        if(playlistChangedListeners.contains(l)) {
            playlistChangedListeners.remove(l);
        }
    }
    public void addOnStateChangedListener(OnStateChangedListener l) {
        if(stateChangedListeners.contains(l))
            return;
        stateChangedListeners.add(l);
    }
    public void removeStateChangedListener(OnStateChangedListener l) {
        if(stateChangedListeners.contains(l))
            stateChangedListeners.remove(l);
    }
    public void addOnNowPlayingListener(OnNowPlayingChangedListener l) {
        if(nowPlayingChangedListeners.contains(l))
            return;
        nowPlayingChangedListeners.add(l);
    }
    public void removeOnNowPlayingListener(OnNowPlayingChangedListener l) {
        if(nowPlayingChangedListeners.contains(l)) {
            nowPlayingChangedListeners.remove(l);
        }
    }
    private void nowPlayingChanged() {
        for(OnNowPlayingChangedListener l: nowPlayingChangedListeners) {
            synchronized (lockObj) {
                waitForTransition();
                if(mState == PlayerState.STOPPED) {
                    l.onNowPlayingChanged(null);
                }else{
                    if (currentIndex < playlist.size()) {
                        l.onNowPlayingChanged(playlist.get(currentIndex));
                    }
                }
            }
            SongInfo currentSong = null;
            if(currentIndex < playlist.size()) {
                currentSong = playlist.get(currentIndex);
            }
            if(currentSong != null) {
                Drawable d = getApplicationContext().getResources().getDrawable(android.R.drawable.sym_def_app_icon);
                Bitmap b = ((BitmapDrawable)d).getBitmap();

                Intent intent = new Intent(getApplicationContext(), AndroidMediaPlayer.class);
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
                Notification n = new Notification.Builder(getApplicationContext())
                        .setContentTitle(currentSong.getTrackName())
                        .setContentText(currentSong.getArtist())
                        .setSmallIcon(android.R.drawable.btn_radio)
                        .setLargeIcon(b)
                        .setContentIntent(pi)
                        .getNotification();
                startForeground(NOW_PLAYING_NOTIFICATION_ID, n);
            } else {
                stopForeground(true);
            }
        }
    }

    @Override
    public void onCompletion(MediaPlayer player) {
        next();
    }
    public void prev() {
        synchronized (lockObj) {
            if(mState != PlayerState.STOPPED) {
                stop();
            }
            currentIndex--;
            if(currentIndex < 0) {
                currentIndex = 0;
                ChangeState(PlayerState.STOPPED);
                return;
            }

            if(currentIndex < playlist.size()) {
                play();
            } else {
                // We have finished
                freeMediaPlayer();
                ChangeState(PlayerState.IDLE);
            }
        }
    }

    public void next()  {
        synchronized (lockObj) {
            if(mState != PlayerState.STOPPED) {
                stop();
            }
            if(!mRepeatSong)
                currentIndex++;
            if(currentIndex >= playlist.size() && mRepeatList)
                currentIndex = 0;

            if(currentIndex < playlist.size()) {
                play();
            } else {
                // We have finished
                freeMediaPlayer();
                ChangeState(PlayerState.IDLE);
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer player, int i, int i2) {
        Log.e(TAG, String.format("Error (%d, %d)", i , i2));
        return false;
    }

    /**
     *  The PlayerState is the internal representation of the state of the player.  Note that
     *  this is different (although related) from the state of the MediaPlayer class.  This
     *  state is based on the playlist of media rather than the individual song. The following
     *  state transitions are valid
     *
     *  STOPPED -> PREPARING: When Play() is called
     *  PREPARING -> PLAY:    When finished preparing
     *  PREPARING -> PAUSE;   If Pause() called while in the preparing state
     *  PREPARING -> STOPPED: If Stop() called in the preparing state
     *  PLAY -> PAUSED:  When Pause() is called
     *  PLAY -> STOPPED: When Stop() is called, when the last song in the list is finished playing
     *  PAUSED -> PLAY:  When Play() is called
     *  PAUSED -> STOP:  When Stop() is called
     */
    public enum PlayerState {
        /**
         *  This is the default state.  No media is being played, either it hasn't been started,
         *  it has reached the end of the playlist and is not repeating or it has been explicitly
         *  commanded to stop.  When commanded to play, playback will begin with the beginning
         *  of the song pointed to by currentIndex
         */
        STOPPED,
        PREPARING,
        /**
         * This state means that there is currently a song playing.
         */
        PLAYING,
        /**
         * This stats means that playback has stopped in the middle of a song.  The difference
         * between this state and STOPPED is that when playback commences it will start at the
         * point where it last left off.
         */
        PAUSED,
        STOPPING,
        IDLE
    }

    // Private Fields
    private boolean mRepeatList = false;
    private boolean mRepeatSong = false;
    private final Object lockObj = new Object();
    private static final String TAG = "MediaPlayerService";
    private ArrayList<SongInfo> playlist;
    private int currentIndex;
    private MediaPlayer mPlayer;
    /**
     * The current state of this class.  This is also what should be used to synchronize access to
     * internal variables.
     */
    private PlayerState mState;
    public PlayerState getPlayerState() {
        return mState;
    }

    public void waitForState(PlayerState requestedState, long mills) throws TimeoutException {
        long start = System.currentTimeMillis();
        while(mState != requestedState) {
            long now = System.currentTimeMillis();
            if(now - start > mills) {
                throw new TimeoutException("Timeout waiting for media player to reach state " +
                        requestedState);
            }
            synchronized (lockObj) {
                try {
                    lockObj.wait(mills);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This function causes the current thread to sleep while the transitional states are happening
     */
    public void waitForTransition() {
        synchronized (lockObj) {
            while((mState == PlayerState.PREPARING) || (mState == PlayerState.STOPPING)) {
                try {
                    mState.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void ChangeState(PlayerState newState) {
        boolean changed;
        synchronized (lockObj) {
            Log.d(TAG, String.format("Moving from %s to %s", mState, newState));
            changed = newState != mState;
            mState = newState;

            lockObj.notifyAll();
        }
        if(changed) {
            for (OnStateChangedListener l : stateChangedListeners) {
                l.onStateChanged(mState);
            }
        }
    }

    public void release() {
        if(mPlayer != null) {
            mPlayer.release();
        }
    }

    AudioManager am = null;

    @Override
    public void pause() {
        if(mState != PlayerState.PLAYING)
            return;

        MediaPlayer p = getMediaPlayer();
        p.pause();
        ChangeState(PlayerState.PAUSED);
    }

    public boolean play() {
        return play(currentIndex);
    }

    public void stop() {
        if((mState == PlayerState.PLAYING) || (mState == PlayerState.PAUSED)) {
            try {
                getMediaPlayer().stop();
            } catch (IllegalStateException ex) {
                getMediaPlayer().reset();
            }
            ChangeState(PlayerState.STOPPED);

            // If we are still in the stopped state after 1 second, free the media player
            Timer releaseTimer = new Timer();
            releaseTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (lockObj) {
                        if(mState == PlayerState.STOPPED) {
                            freeMediaPlayer();
                            stopForeground(true);
                        }
                    }
                }
            }, 1000);
        }
    }

    /**
     * Function to be called if a Media Player is required.  If the player does not exist a new one
     * will be created and returned to the user with all the callbacks registered.  If the player
     * already exists, then it will be returned immediately.  This is the only function in this
     * class that should ever create a new MediaPlayer object.
     *
     * @return The media player to be acted upon.
     */
    private MediaPlayer getMediaPlayer() {
        synchronized (lockObj) {
            if(mPlayer == null) {
                mPlayer = new MediaPlayer();
                mPlayer.setOnPreparedListener(this);
                mPlayer.setOnErrorListener(this);
                mPlayer.setOnCompletionListener(this);
            }
            return mPlayer;
        }
    }

    private void freeMediaPlayer() {
        synchronized (lockObj) {
            if(mPlayer != null) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
                ChangeState(PlayerState.STOPPED);
            }
        }
    }

    @Override
    public int getCurrentPosition() {
        int r;
        synchronized (lockObj) {
            waitForTransition();
            if(mState == PlayerState.STOPPED) {
                r = 0;
            } else {
                r = getMediaPlayer().getCurrentPosition();
            }
        }
        Log.d(TAG, String.format("Current Song Position: %d", r));
        return r;
    }

    @Override
    public void seekTo(int milliseconds) {
        synchronized (lockObj) {
            if(mPlayer != null) {
                mPlayer.seekTo(milliseconds);
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer p) {
        synchronized (lockObj) {
            switch(mState) {
                case PREPARING:
                    // Make sure we have focus before actually playing music
                    int result = am.requestAudioFocus(onAudioFocusChangeListener,
                            AudioManager.STREAM_MUSIC,
                            AudioManager.AUDIOFOCUS_GAIN
                    );
                    if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                        p.start();
                    else
                        Log.e(TAG, "Could not gain audio focus!");

                    ChangeState(PlayerState.PLAYING);
                    nowPlayingChanged();
                    break;
                default:
                    Log.w(TAG, "onPrepared was called, but we were no longer in the PREPARING state");
            }
        }
    }
    AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if(focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT)
                pause();
            else if(focusChange == AudioManager.AUDIOFOCUS_GAIN)
                synchronized (lockObj) {
                    if(mState == PlayerState.PAUSED)
                        play();
                    getMediaPlayer().setVolume(1.0f, 1.0f);
                }
            else if(focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                stop();
                am.abandonAudioFocus(onAudioFocusChangeListener);
            } else if(focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                if(getMediaPlayer().isPlaying()) {
                    getMediaPlayer().setVolume(0.1f, 0.1f);
                }
            }
        }
    };
    /**
     * Plays the song specified in the index
     *
     * If the player is in the stopped state, starts a new song. If a song is playing, nothing
     * happens if the requested song is the same as the current song, if the requested song is
     * different, the current song stops and a new song is played.  If paused and the requested index
     * is the same as the current index the song starts where it left off, if it is different,
     * then the new song is started.
     */
    public boolean play(int index) {
        Log.d(TAG, "Play(" + index + ") called");
        boolean loaded = false;
        MediaPlayer mediaPlayer = getMediaPlayer();

        // If we are in the preparing state, we MUST wait until we have transitioned out.
        synchronized (lockObj) {
            waitForTransition();
            switch (mState) {
                case STOPPED:
                    mediaPlayer.reset();
                    break;
                case PLAYING:
                    if (mediaPlayer.isPlaying()) {
                        if (index == currentIndex)
                            return false;
                        else {
                            mediaPlayer.reset();
                        }
                    }
                    break;
                case PAUSED:
                    if (index == currentIndex) {
                        mediaPlayer.start();
                        ChangeState(PlayerState.PLAYING);
                        return true;
                    } else {
                        mediaPlayer.reset();
                    }
                    break;
            }

            currentIndex = index;
            try {
                mediaPlayer.setDataSource(playlist.get(currentIndex).getFilename());
                loaded = true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, String.format("Could not load %s", playlist.get(currentIndex)));
                currentIndex++;
            }

            if (!loaded) {
                ChangeState(PlayerState.STOPPED);
            } else {
                mediaPlayer.prepareAsync();
                ChangeState(PlayerState.PREPARING);
            }

        }
        return loaded;
    }
}
