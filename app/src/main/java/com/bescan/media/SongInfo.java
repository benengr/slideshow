package com.bescan.media;

/**
 * This class packages information about a Song
 * Created by banderson on 9/29/14.
 */
public class SongInfo {

    private String filename;
    private String artist;
    private String trackName;
    private String album;
    private int duration;
    private long id;
    public void setId(long newId) {
        id = newId;
    }
    public long getId() {
        return id;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int d) {
        duration = d;
    }
    public int getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    private int trackNumber;
}
