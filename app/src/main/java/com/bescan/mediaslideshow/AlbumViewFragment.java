package com.bescan.mediaslideshow;

import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.bescan.media.SongInfo;
import com.bescan.media.player.service.AndroidMediaPlayer;
import com.bescan.mediaslideshow.R;
import com.bescan.mediaslideshow.loader.AlbumListAdapter;

import java.util.ArrayList;

/**
 * The AlbumViewFragment is a fragment that displays the list of Albums that the user is
 * able to access.
 *
 * Created by banderson on 10/1/14.
 */
public class AlbumViewFragment extends ListFragment {
    AndroidMediaPlayer service;
    private static final String TAG = "AlbumViewFragment";
    private final Object lock = new Object();
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent i = new Intent(getActivity(), AndroidMediaPlayer.class);
        getActivity().bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        setListAdapter(new AlbumListAdapter(getActivity(), getLoaderManager()));
    }

    @Override
    public void onStop() {
        super.onStop();
        synchronized (lock) {
            if (service != null) {
                getActivity().unbindService(mConnection);
                service = null;
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.v(TAG, "Album Clicked at position " + position);
        synchronized (lock) {
            if (service != null) {
                String columns[] = {
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ALBUM,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media.TRACK,
                        MediaStore.Audio.Media.DURATION
                };
                String where = MediaStore.Audio.Media.ALBUM + "=? AND " + MediaStore.Audio.Media.DATA + " <> ''";
                TextView albumView = (TextView) v.findViewById(R.id.firstLine);
                String whereval[] = {(String) albumView.getText()};
                String orderBy = MediaStore.Audio.Media.TRACK;

                Cursor c = getActivity().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        columns, where, whereval, orderBy);

                ArrayList<SongInfo> songs = new ArrayList<SongInfo>();

                while (c.moveToNext()) {

                    SongInfo t = new SongInfo();
                    t.setTrackName(c.getString(c.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    t.setTrackNumber(c.getInt(c.getColumnIndex(MediaStore.Audio.Media.TRACK)));
                    t.setFilename(c.getString(c.getColumnIndex(MediaStore.Audio.Media.DATA)));
                    t.setAlbum(c.getString(c.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
                    t.setArtist(c.getString(c.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    t.setDuration(c.getInt(c.getColumnIndex(MediaStore.Audio.Media.DURATION)));
                    songs.add(t);
                }

                if (songs.size() > 0) {
                    service.removeAllSongs();
                    service.addSongs(songs);
                }
            }
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.v(TAG, "Bound to Service");
            synchronized (lock) {
                AndroidMediaPlayer.LocalBinder b = (AndroidMediaPlayer.LocalBinder) iBinder;
                service = b.getService();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            synchronized(lock) {
                service = null;
            }
        }
    };

}
