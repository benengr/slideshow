package com.bescan.mediaslideshow;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.bescan.mediaslideshow.R;

/**
 * Created by banderson on 10/17/14.
 */
public class TestFragmentActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_fortests);
    }
}
