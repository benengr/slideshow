package com.bescan.mediaslideshow;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bescan.media.browser.MediaBrowserFragment;
import com.bescan.media.player.nowplaying.NowPlayingFragment;
import com.bescan.media.player.playlist.PlaylistFragment;
import com.bescan.media.player.service.AndroidMediaPlayer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class MainActivity extends ActionBarActivity {
    AndroidMediaPlayer service;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate() Called");
        super.onCreate(savedInstanceState);

        // Initialize the image loader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();
        ImageLoader.getInstance().init(config);
        setContentView(R.layout.navigation_drawer);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.browser_container, MediaBrowserFragment.newInstance("T"), MediaBrowserFragment.class.getCanonicalName())
                    .commit();
        }

        String[] mDrawerLabels = getResources().getStringArray(R.array.drawer_items);
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                        R.layout.drawer_list_item, mDrawerLabels)
        );
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent i = new Intent(this, AndroidMediaPlayer.class);
        startService(i);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (service != null) {
            Log.v(TAG, "Unbinding Service");
            unbindService(mConnection);
            service = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Log.d(TAG, item.getTitle().toString() + "clicked");
        int id = item.getItemId();
        if(id == R.id.action_settings) {
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String [] projection = new String[3];
            projection[0] = MediaStore.Audio.Media._ID;
            projection[1] = MediaStore.Audio.Media.TITLE;
            projection[2] = MediaStore.Audio.Media.DATA;
            String selection = MediaStore.Audio.Media.DATA + " = ?";
            String [] args = new String[1];
            args[0] = "";
            Cursor cursor = getContentResolver().query(uri, projection , selection, args, null);
            Log.d(TAG, "Found " + cursor.getCount() + " items without filenames");
            cursor.moveToFirst();
            int count = 0;
            while(!cursor.isAfterLast()) {
                int thisId = cursor.getInt(0);
                Uri thisUri = ContentUris.withAppendedId(uri, thisId);
                getContentResolver().delete(thisUri, null, null);
                cursor.moveToNext();
                count++;
                if(count % 100 == 0) {
                    Log.d(TAG, "Removed " + count + " items");
                }
            }
            cursor.close();

        }
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        private static final String TAG = "MainActivity-ServiceConnection";
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.v(TAG, "Service Connected");
            AndroidMediaPlayer.LocalBinder b = (AndroidMediaPlayer.LocalBinder)iBinder;
            service = b.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.v(TAG, "Service Disconnected");
        }
    };


    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            String label = ((TextView)view).getText().toString();
            selectItem(label);
        }

        private void selectItem(String label) {
            Fragment f;
            Class c = null;
            FragmentManager fm = getSupportFragmentManager();
            if(label.equals(getString(R.string.nav_library))) {
                c = MediaBrowserFragment.class;
            } else if (label.equals(getString(R.string.nav_playlist))) {
                c = NowPlayingFragment.class;
            } else if(label.equals(getString(R.string.nav_queue))) {
                c = PlaylistFragment.class;
            } else if(label.equals(getString(R.string.nav_visualizer))) {
                c = null;
            }

            if(c != null) {
                f = fm.findFragmentByTag(c.getCanonicalName());
                if (f == null) {
                    try {
                        f = (Fragment)c.newInstance();
                    } catch (Exception e) {
                        Log.e(TAG, "Error creating instance of fragment", e);
                    }
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.browser_container, f, c.getCanonicalName());
                transaction.commit();
            }


        }
    }
}
