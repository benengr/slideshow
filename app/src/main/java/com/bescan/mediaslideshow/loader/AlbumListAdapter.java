package com.bescan.mediaslideshow.loader;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import com.bescan.mediaslideshow.R;

/**
 *
 * Created by banderson on 10/1/14.
 */
public class AlbumListAdapter extends SimpleCursorAdapter implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "AlbumListAdapter";
    private Context mContext;
    private static final String projection[] = {
            MediaStore.Audio.Albums._ID,
            MediaStore.Audio.Albums.ALBUM,
            MediaStore.Audio.Albums.ARTIST,
            MediaStore.Audio.Albums.NUMBER_OF_SONGS
    };
    private static final String sortOrder = MediaStore.Audio.Albums.ALBUM + " ASC";
    public static final int LOADER_ID = 100;
    public AlbumListAdapter(Context c, LoaderManager manager) {
        super(c, R.layout.list_item_view,
                null, new String[] {MediaStore.Audio.Albums.ALBUM, MediaStore.Audio.Albums.ARTIST},
                new int [] {R.id.firstLine, R.id.secondLine}, 0 );
        mContext = c;
        manager.initLoader(AlbumListAdapter.LOADER_ID, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.v(TAG, "onCreateLoader called");
        return new CursorLoader(mContext,
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                projection, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.v(TAG, "onLoadFinished called");
        swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        Log.v(TAG, "onLoaderReset() called");
        swapCursor(null);
    }

}
