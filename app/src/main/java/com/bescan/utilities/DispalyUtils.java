package com.bescan.utilities;

import android.util.DisplayMetrics;

/**
 * Utilties used for display information
 * Created by banderson on 1/15/15.
 */
public class DispalyUtils {
    public static enum ScreenType implements Comparable<ScreenType>{
        EXTRA_LARGE(9.5f),
        TABLET(6.75f),
        PHONE(4.0f),
        SMALL_PHONE(3.0f),
        EXTRA_SMALL(0f);

        float mSize = 0;
        ScreenType(float size) {
            mSize = size;
        }

        public float getSize() {
            return mSize;
        }
    }
    public static double getDiagnalSize(DisplayMetrics dm) {
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int dens = dm.densityDpi;

        double wi = (double)width/(double)dens;
        double hi = (double)height/(double)dens;
        return Math.sqrt(wi * wi + hi * hi);

    }
}
