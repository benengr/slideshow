package com.bescan.utilities;

import android.util.Log;

import java.util.HashMap;

/**
 * The IdManager class provides an Application global way to manage Id.  Specifically used to manage
 * loader ids
 *
 * Created by banderson on 10/20/14.
 */
public class IdManager {
    private static HashMap<Class, IdManager> managerTable = new HashMap<Class, IdManager>();
    private static int currentBase = 0;
    private static final int groupSize = 0x00001000;
    private static final String TAG = "IdManager";

    public static IdManager getInstance(Class managerFor) {
        synchronized (globalLock) {
            IdManager instance = managerTable.get(managerFor);
            if (instance == null) {
                Log.d(TAG, String.format("Created Id: %d for class %s", currentBase, managerFor.toString()));
                instance = new IdManager(currentBase);
                currentBase += groupSize;
                managerTable.put(managerFor, instance);
            }
            return instance;
        }
    }

    private int mNextId;
    private static final Object globalLock = new Object();
    private final Object lock = new Object();
    private HashMap<String, Integer> idTable = new HashMap<String, Integer>();
    private IdManager(int base) {
        mNextId = base;
    }

    public Integer getId(String hash) {
        synchronized (lock) {
            Integer id = idTable.get(hash);
            if (id == null) {
                id = mNextId;
                idTable.put(hash, id);
                mNextId++;
            }
            return id;
        }
    }
}

